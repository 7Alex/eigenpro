if __name__ == '__main__':
    from sklearn.datasets import fetch_mldata
    from fast_kernel_classification import FastKernelClassification
    import time
    import numpy as np

    print("Fetching Data")
    mnist = fetch_mldata('MNIST original', data_home="skmnist")
    mnist.data = mnist.data / 255.

    train_size = 60000
    test_size = 10000

    p = np.random.permutation(60000)
    x_train = mnist.data[p][:train_size]
    y_train = mnist.target[p][:train_size]
    x_test = mnist.data[60000:60000 + test_size]
    y_test = mnist.target[60000:60000 + test_size]

    estimator = FastKernelClassification(n_epoch=1, kernel="gaussian", bandwidth=5, dtype=np.float32, random_state=1)

    print("Fitting Model")
    stime = time.time()
    estimator.fit(x_train, y_train)
    etime = time.time()
    print("Fit Time " + str(etime - stime))

    print("Calculating Error")
    y_pred_train = estimator.predict(x_train)
    # y_pred_test = estimator.predict(x_test)
    print("Test data result: ")
    # print(str(100. * np.sum(y_pred_test != y_test) / len(y_test)) + "% error")
    print(str(100. * np.sum(y_pred_train != y_train) / len(y_train)) + "% error")
