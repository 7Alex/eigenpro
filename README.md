**Fast Kernel Regression/Classification**

A simple and robust kernel method that automatically calculates optimal parameters for very fast training.

---

## Quick Shell Script

For a quick test on the MNIST dataset, execute the following command in a bash shell,

>
```
python run_mnist.py
```


---

## Interactive Python Console

When using a Python conosle, we can create a fast kernel regressor/classifier in the following way,

>
```python
from fast_kernel_regression import FastKernelRegression
import numpy as np
n_samples, n_features, n_targets = 4000, 20, 3
rng = np.random.RandomState(1)
x_train = rng.randn(n_samples, n_features)
y_train = rng.randn(n_samples, n_targets)
rgs = FastKernelRegression(n_epoch=3, bandwidth=1)
rgs.fit(x_train, y_train)
y_pred = rgs.predict(x_train)
loss = np.mean(np.square(y_train - y_pred))
```

>
```python
from fast_kernel_classification import FastKernelClassification
import numpy as np
n_samples, n_features, n_classes = 4000, 20, 3
rng = np.random.RandomState(1)
x_train = rng.randn(n_samples, n_features)
y_train = rng.choice(n_classes, n_samples)
clf = FastKernelClassification(n_epoch=3, bandwidth=1)
clf.fit(x_train, y_train)
y_pred = clf.predict(x_train)
acc = np.mean(y_train == y_pred)
```
